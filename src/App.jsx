import { CounterApp } from './components/CounterApp'
import { CounterAppHooks } from './components/CounterAppHooks'
import { FirstApp } from './components/FirstApp'
import { FormWithCustomHooks } from './components/FormWithCustomHooks'

import { GifsApp } from "./components/GifsApp"
import { Padre } from './components/09-Homework/Padre'
import { MemoCallback } from './components/MemoCallback'
import { MultipleCustomHooks } from './components/MultipleCustomHooks'
import { SimpleForm } from './components/SimpleForm'

function App() {
  return (
    <>
      <Padre />
      <MemoCallback />
      <MultipleCustomHooks />
      <FormWithCustomHooks />
      <SimpleForm />
      <CounterAppHooks />
      <FirstApp name="Goku" />
      <CounterApp value={ 10 } />
      <GifsApp />
    </>
  )
}

export default App
