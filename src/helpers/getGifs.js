export const getGifs = async ( category ) => {
  // const apiKey = import.meta?.env?.VITE_GIPHY_API_KEY
  const apiKey = '6a5VvgJFtqwKzGOzWpp7eoGfqlPgLaBT'
  const url = `https://api.giphy.com/v1/gifs/search?api_key=${ apiKey }&q=${ category }&limit=5`
  const resp = await fetch( url )
  const { data } = await resp.json()
  return data.map(gif => ({
    id: gif.id,
    title: gif.title,
    url: gif.images.downsized_medium.url,
  }))
}