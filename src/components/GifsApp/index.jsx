import { useState } from "react"
import { AddCategory } from "./AddCategory"
import { GifsGrid } from "./GifsGrid"

export const GifsApp = () => {
  const [categories, setCategories] = useState(['Gohan'])
  const onAddCategory = (newCategory) => {
    if (categories.includes(newCategory)) return

    setCategories([newCategory, ...categories])
  }
  return (
    <>
      <h1>GifsApp</h1>
      <AddCategory onAddCategory={ onAddCategory } />
      { categories.map((category) => (
        <GifsGrid key={ category } category={ category} />
      )) }
    </>
  )
}
