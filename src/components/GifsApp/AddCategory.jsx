import { useState } from 'react'
import PropTypes from 'prop-types'

export const AddCategory = ({ onAddCategory }) => {
  const [inputValue, setInputValue] = useState('')
  
  const onInputChange = ({ target }) => {
    setInputValue(target.value)
  }
  // const onSubmit = (event) => {
  //   event.preventDefault()
  //   if (inputValue.trim().length < 1) return
  //   setCategories(categories => [...categories, inputValue])
  //   setInputValue('')
  // }
  const onSubmit = (event) => {
    event.preventDefault()
    if (inputValue.trim().length < 1) return
    onAddCategory(inputValue)
    setInputValue('')
  }
  return (
    <form onSubmit={ onSubmit } aria-label="category-form">
      <input
        type="text"
        placeholder="Gifs"
        value={ inputValue }
        onChange={ onInputChange }
      />
    </form>
  )
}

AddCategory.propTypes = {
  onAddCategory: PropTypes.func.isRequired
}