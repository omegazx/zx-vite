import { PropTypes } from 'prop-types';
import { useCounter } from '../hooks/useCounter';


export const CounterAppHooks = ({ value }) => {
  const { counter, increment, decrement, reset } = useCounter(value)

  return (
    <>
      <h1>Counter App - Hooks</h1>
      <h2>{ counter }</h2>
      <button onClick={ () => increment() }>+1</button>
      <button onClick={ () => decrement() }>-1</button>
      <button aria-label='reset-button' onClick={ () => reset(value) }>Reset</button>
    </>
  )
}
