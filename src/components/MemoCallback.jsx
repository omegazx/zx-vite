import { useMemo, useState } from 'react'
import { useCounter } from '../hooks/useCounter'

const extraMethod = (times) => {
  for (let i = 0; i < times; i++) {
    console.log('XD')
  }
  return `Renderizado!`
}


export const MemoCallback = () => {
  const { counter, increment } = useCounter(0)
  const [show, setShow] = useState(true)
  
  const memorizedValue = useMemo( () => extraMethod(counter), [ counter ] )
  // useCallback to memorize methods
  // setCounter((value) => value + 1)

  return (
    <>
      <h1>Memo Callbacks. Counter: { counter }</h1>
      <button onClick={ () => setShow(!show) }>SHOW increaser</button>
      { show && <button onClick={ increment }>+1</button> }
      <p>{ memorizedValue }</p>
    </>
  )
}
