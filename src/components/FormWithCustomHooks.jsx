import { useRef } from "react"
import { useForm } from "../hooks/useForm"

export const FormWithCustomHooks = () => {
  const { formState, onInputChange, onResetForm, username, email, password } = useForm({
    username: 'zaydzx',
    email: 'zx@gmail.com',
    password: '',
  })
  
  const emailRef = useRef()
  const onClickChangeEmail = () => {
    emailRef.current.select()
  }

  return (
    <>
      <h1>Form - Custom hooks</h1>
      <input onChange={ onInputChange } value={ username } name="username" />
      <input onChange={ onInputChange } value={ email } name="email" type="email" ref={ emailRef } />
      <input onChange={ onInputChange } value={ password } name="password" type="password" />
      <button onClick={ onResetForm }>RESET</button>
      <button onClick={ onClickChangeEmail }>Change Email</button>
    </>
  )
}
