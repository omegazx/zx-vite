import { PropTypes } from 'prop-types';

export const FirstApp = ({ name }) => {
  return (
    <>
      <h1 data-testid="test-name">Hola, soy { name }</h1>
    </>
  )
}

FirstApp.propTypes = {
  name: PropTypes.string.isRequired
}