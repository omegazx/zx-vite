import React from 'react'
import { useCounter, useFetch } from '../hooks'

export const MultipleCustomHooks = () => {
  const { counter, increment } = useCounter(1)

  const { data, isLoading, hasError } = useFetch(`https://www.breakingbadapi.com/api/quotes/${ counter }`)
  const { author, quote } = !!data && data[0]

  return (
    <>
      <h1>Quotes</h1>
      <hr />
      { isLoading
          ?
            <p>Cargando...</p>
          :
            <p>{ quote }<br />{ author }</p>
      }
      <button onClick={ increment } disabled={ isLoading }>Next quote</button>
    </>
  )
}
