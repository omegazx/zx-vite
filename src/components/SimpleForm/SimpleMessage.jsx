import React, { useEffect } from 'react'
import { useState } from 'react'

export const SimpleMessage = () => {
  const [coords, setCoords] = useState({ x: 0, y: 0 })
  useEffect(() => {
    // console.log('mounted!')
    const onMouseMove = ({ x, y }) => {
      setCoords({ x, y })
    }
    window.addEventListener('mousemove', onMouseMove)
    return () => {
      window.removeEventListener('mousemove', onMouseMove)
      // console.log('unmounted!')
    }
  }, [])
  return (
    <>
      <div>Usuario existente</div>
      { JSON.stringify(coords) }
    </>
  )
}
