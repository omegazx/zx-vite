import { PropTypes } from 'prop-types';
import { useEffect, useState } from 'react';
import { SimpleMessage } from './SimpleMessage';


export const SimpleForm = ({ value }) => {
  const [formState, setFormState] = useState({
    username: '',
    email: '',
  })
  const { username, email } = formState

  const onInputChange = ({ target }) => {
    const { name, value } = target
    setFormState({
      ...formState,
      [name]: value
    })
  }

  useEffect(() => {
    // console.log('mounted!')
  }, [])
  useEffect(() => {
    // console.log('email changed')
  }, [email])
  

  return (
    <>
      <h1>Simple Form</h1>
      <input onChange={ onInputChange } value={ username } name="username" />
      <input onChange={ onInputChange } value={ email } name="email" type="email" />
      { username === 'zaydzx' && <SimpleMessage /> }
    </>
  )
}
