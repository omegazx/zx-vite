import { renderHook, waitFor } from "@testing-library/react"
import { useFetchGifs } from "../../src/hooks/useFetchGifs"

describe('Prueba hook useFetchGifs', () => {
  test('should return initial state', () => {
    const { result } = renderHook( () => useFetchGifs('garou') )
    const { images, isLoading } = result.current
    expect ( images.length ).toBe( 0 )
    expect ( isLoading ).toBeTruthy()
  })
  test('should return images', async () => {
    const { result } = renderHook( () => useFetchGifs('garou') )
    await waitFor(
      () => expect ( result.current.images.length ).toBeGreaterThan( 0 )
    )
    const { images, isLoading } = result.current
    expect ( images.length ).toBeGreaterThan( 0 )
    expect ( isLoading ).toBeFalsy()
  })
})