import { render, screen } from "@testing-library/react"
import { FirstApp } from "../../src/components/FirstApp"


describe('Pruebas en <FirstApp />', () => {
  const name = 'Bryan'
  // test('Match con el snapshot', () => {
  //   const name = 'Bryan'
  //   const { container } = render( <FirstApp name={ name } />)
  //   expect( container ).toMatchSnapshot()
  // })
  test('Nombre en h1', () => {
    const { container, getByText, getByTestId } = render( <FirstApp name={ name } />)
    expect( getByTestId('test-name').innerHTML ).toContain( name )
  })
  test('Nombre en h1 v2', () => {
    render( <FirstApp name={ name } />)
    expect( screen.getByRole('heading', { level: 1 }).innerHTML ).toContain( name )
  })
})