import { render, screen } from "@testing-library/react"
import { GifItem } from "../../../src/components/GifsApp/GifItem"

describe('Pruebas en <GifItem />', () => {
  const gifData = {
    title: 'Saitama',
    url: 'https://opm.com/saitama.png'
  }
  test('SnapShot GifItem', () => {
    const { container } = render( <GifItem { ...gifData } />)
    expect( container ).toMatchSnapshot()
  })
  test('Datos GifItem alt/src', () => {
    render( <GifItem { ...gifData } />)
    const { alt, src} = screen.getByRole('img')
    expect( alt ).toBe( gifData.title )
    expect( src ).toBe( gifData.url )
  })
})