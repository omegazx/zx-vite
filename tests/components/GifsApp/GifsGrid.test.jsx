import { render, screen } from "@testing-library/react"
import { GifsGrid } from "../../../src/components/GifsApp/GifsGrid"
import { useFetchGifs } from "../../../src/hooks/useFetchGifs"

jest.mock('../../../src/hooks/useFetchGifs')

describe('Pruebas en <GifsGrid />', () => {
  const category = 'Saitama'
  
  test('should show loading', () => {
    useFetchGifs.mockReturnValue({
      images: [],
      isLoading: true
    })
    
    render( <GifsGrid category={ category } />)
    screen.getByText( 'Cargando...' )
    screen.getByText( category )
  })
  
  test('should show items', () => {
    useFetchGifs.mockReturnValue({
      images: [
        { id: 'a', title: 'b', url: 'c' }
      ],
      isLoading: false
    })
    
    render( <GifsGrid category={ category } />)
    screen.getByText( category )
    expect( screen.getAllByRole('img').length ).toBe( 1 )
  })
})