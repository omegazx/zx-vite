import { render, fireEvent, screen } from "@testing-library/react"
import { AddCategory } from "../../../src/components/GifsApp/AddCategory"

describe('Pruebas en <addCategory />', () => {

  test('should change box value', () => {
    render(<AddCategory onAddCategory={ () => {} } />)

    const input = screen.getByRole('textbox')
    fireEvent.input( input, { target: { value: 'saitama' } } )

    expect( input.value ).toBe( 'saitama' )
  })

  test('should submit form with text', () => {
    const inputValue = 'saitama'
    const onAddCategory = jest.fn()

    render(<AddCategory onAddCategory={ onAddCategory } />)

    const input = screen.getByRole('textbox')
    fireEvent.input( input, { target: { value: inputValue } } )

    const form = screen.getByRole('form')
    fireEvent.submit( form )

    expect( input.value ).toBe( '' )
    expect( onAddCategory ).toHaveBeenCalled()
    expect( onAddCategory ).toHaveBeenCalledTimes( 1 )
    expect( onAddCategory ).toHaveBeenCalledWith( inputValue )
  })

  test('should submit form without text', () => {
    const onAddCategory = jest.fn()

    render(<AddCategory onAddCategory={ onAddCategory } />)

    const form = screen.getByRole('form')
    fireEvent.submit( form )

    expect( onAddCategory ).not.toHaveBeenCalled()
    // expect( onAddCategory ).toHaveBeenCalledTimes( 0 )
  })
})