import { fireEvent, render, screen } from "@testing-library/react"
import { CounterApp } from "../../src/components/CounterApp"


describe('Pruebas en <CounterApp />', () => {
  const startValue = 100
  test('Valor inicial de 100', () => {
    render( <CounterApp value={ startValue } />)
    expect( screen.getByText(startValue)).toBeTruthy()
  })
  test('Valor +1', () => {
    render( <CounterApp value={ startValue } />)
    fireEvent.click( screen.getByText('+1') )
    expect( screen.getByText(startValue + 1)).toBeTruthy()
  })
  test('Valor -1', () => {
    render( <CounterApp value={ startValue } />)
    fireEvent.click( screen.getByText('-1') )
    expect( screen.getByText(startValue - 1)).toBeTruthy()
  })
  test('Reset 100', () => {
    render( <CounterApp value={ startValue } />)
    fireEvent.click( screen.getByText('+1') )
    fireEvent.click( screen.getByText('+1') )
    fireEvent.click( screen.getByText('+1') )
    fireEvent.click( screen.getByRole('button', { name: 'reset-button' }) )
    expect( screen.getByText(startValue)).toBeTruthy()
  })
})